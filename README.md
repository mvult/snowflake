We're trying to download segment, sub-packages and packages from snowflake.

First, lets see the schemas and such to see what's available. 

- So, there's a table called ZEFR_PACKAGES_NAME.  Videos likely use this a foreign ID. 
- However, it seems to not match up/be smaller than the P/SP/S schema we are used to. 
	- PAYLOAD_RELEVANT_TOPIC_IDS
	- PAYLOAD__DERIVED__CATEGORIZER_CONTENT_HASH
	- PAYLOAD__DERIVED__TAGS
	- PAYLOAD__DERIVED__PACKAGE_IDS

- within Stagint there are interesting APP tables
	STAGING.APPS_CPT_PACKAGE_DEFINITION has 270K packages 
	- Package ID 16825 is top trending
- STAGING.APPS_CPT_CPT_SEARCHES has the actual JSON search terms.
"ecf2b178-6a31-11ea-9ce3-0242ac110003"

- Seems to be a vague schema where segments, sub-packages and packages are all the same thing, and all can have child packages.  Try to download music and see if there's anything that's not here. 
- Find the largest music package
- Download it.  See if:
	- Has smaller packages within it that have novel videos.
	- reasonable overall size


So 'All music', which is the largest music package, does show up SELECT * FROM PROD.REPORTING.ZEFR_PACKAGES_NAME WHERE LOWER(PACKAGE_NAME) LIKE '%all music%'; 
	- ID 1439
	- Probably pull from reporting.payload_derived_tags or reporting.payload_derived_package_ids

Things in video 