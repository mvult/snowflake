import csv
import os

def valid_channel(channelID):
    if not isinstance(channelID, str): return False
    if len(channelID) != 24: return False
    if channelID[:2] != "UC": return False
    return True

def build_channel_list(text):
    channels = []

    with open(text.split(" ")[1], "r", encoding="utf-8") as f:
        csvRdr = csv.reader(f)
        first = True

        for i, row in enumerate(csvRdr):
            if i == 0:
                continue
            if valid_channel(row[0]): channels.append(row[0])

    return channels

def get_channel_videos(QM, filename):
    channels = build_channel_list(filename)

    path_parts = os.path.splitext(filename)

    query = 'SELECT PAYLOAD__CONTENT_ID  AS "yt_video_id", 0 AS "relevancy_score", PAYLOAD__METRICS__VIEWS AS "views", 0 AS "likes", 0 AS "dislikes", 0 AS "comments", PAYLOAD__TITLE AS "title", PAYLOAD__PUBLISHED_AT AS "publish_ts", PAYLOAD__DURATION AS "duration", PAYLOAD__CHANNEL_ID AS "yt_channel_id", PAYLOAD__DESCRIPTION  AS "description", 0 AS "yt_category_id", 0 AS "is_zefr_claimed", PAYLOAD__DERIVED__AVERAGE_FORECAST AS "forecast", 0 AS "yt_channel_title", \'CUSTOM_RESEARCH_PULL\' AS "hydra_job_id" FROM PROD.REPORTING.LATEST_MONETIZABLE WHERE PAYLOAD__CHANNEL_ID IN ({}) LIMIT 2000000;'.format(', '.join(["'{}'".format(c) for c in channels]))

    QM.make_query(query, "Channel Videos {}.csv".format(path_parts[0]))

def get_channel_stats(QM, filename):
    channels = build_channel_list(filename)

    path_parts = os.path.splitext(filename)

    query = 'SELECT PAYLOAD__CHANNEL_ID AS "yt_channel_id", PAYLOAD__CHANNEL_TITLE as "yt_channel_title", MAX(PAYLOAD__METRICS__CHANNEL_METRICS__VIEWS) as "channel_views", MAX(PAYLOAD__METRICS__CHANNEL_METRICS__VIDEOS) as "channel_videos", MAX(PAYLOAD__METRICS__CHANNEL_METRICS__SUBSCRIBERS) as "subscribers", COUNTRY AS "country" FROM PROD.REPORTING.LATEST_MONETIZABLE AS m JOIN PROD.ANALYST.CHANNEL_STATS AS c ON m.PAYLOAD__CHANNEL_ID = c.CHANNEL_ID WHERE PAYLOAD__CHANNEL_ID IN ({}) GROUP BY PAYLOAD__CHANNEL_ID, PAYLOAD__CHANNEL_TITLE, COUNTRY LIMIT 2000000;'.format(', '.join(["'{}'".format(c) for c in channels]))

    QM.make_query(query, "Channel Stats {}.csv".format(path_parts[0]))
            
