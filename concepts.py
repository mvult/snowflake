import unicodedata
import re

import click

from package import Package




def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')

def search_concepts(QM, search_term):
    query = "SELECT * FROM PROD.REPORTING.LATEST_INVENTORY_CONCEPT_VERSION WHERE LOWER(IC_NAME) LIKE '%{}%' OR LOWER(ICV_NAME) LIKE '%{}%';".format(search_term.lower(), search_term.lower())
    rows = QM.make_query(query)

    ret = []
    id = 1
    for r in rows:
        ret.append(Package(ICV_UUID= r[6],
            INTERNAL_CONCEPT_ID = 'C'+str(id),
            IC_UUID = r[2],
            isConcept = True,
            icName = r[1],
            icvName = r[5],
            beta = r[9],
            primary = r[10],
            retired = r[11]))
        id = id + 1

    if len(ret) > 400:
        print("Warning, over 400 packages.  This is weird.  Did you search right?")

    ret = sorted(ret, key= lambda x: x.icName)

    return ret


def download_concept(QM, concept_id, concept_name, min_score, country=None, limit=2000000):
    if min_score < 0.5 or min_score > 1.0:
        raise Exception("Min score should be at minimum 0.5 or maximum of 010")

    if limit is None:
        limit = 2000000
    
    if country:
        #Currently only one type of arg
        subquery = '(SELECT CHANNEL_ID FROM PROD.ANALYST.CHANNEL_STATS WHERE COUNTRY = \'{}\')'.format(country)

        query = 'SELECT m.PAYLOAD__CONTENT_ID  AS "yt_video_id", 0 AS "relevancy_score", m.PAYLOAD__METRICS__VIEWS AS "views", COALESCE(m.PAYLOAD__METRICS__LIKES, 0) AS "likes", COALESCE(m.PAYLOAD__METRICS__DISLIKES, 0) AS "dislikes", COALESCE(m.PAYLOAD__METRICS__COMMENTS, 0) AS "comments", m.PAYLOAD__TITLE AS "title", m.PAYLOAD__PUBLISHED_AT AS "publish_ts", m.PAYLOAD__DURATION AS "duration", m.PAYLOAD__CHANNEL_ID AS "yt_channel_id", m.PAYLOAD__DESCRIPTION  AS "description", 0 AS "yt_category_id", 0 AS "is_zefr_claimed", m.PAYLOAD__DERIVED__AVERAGE_FORECAST AS "forecast", m.PAYLOAD__CHANNEL_TITLE AS "yt_channel_title", s.TAXONOMY_UUID AS "hydra_job_id", s.SCORE as "score" FROM PROD.REPORTING.LATEST_SCORES AS s JOIN PROD.REPORTING.LATEST_MONETIZABLE as m ON s.CONTENT_ID = m.PAYLOAD__CONTENT_ID WHERE TAXONOMY_UUID = \'{}\' AND s.SCORE > {} AND m.PAYLOAD__CHANNEL_ID IN {} LIMIT {};'.format(concept_id, min_score, subquery, limit)
    else:
        query = 'SELECT m.PAYLOAD__CONTENT_ID  AS "yt_video_id", 0 AS "relevancy_score", m.PAYLOAD__METRICS__VIEWS AS "views", COALESCE(m.PAYLOAD__METRICS__LIKES, 0) AS "likes", COALESCE(m.PAYLOAD__METRICS__DISLIKES, 0) AS "dislikes", COALESCE(m.PAYLOAD__METRICS__COMMENTS, 0) AS "comments", m.PAYLOAD__TITLE AS "title", m.PAYLOAD__PUBLISHED_AT AS "publish_ts", m.PAYLOAD__DURATION AS "duration", m.PAYLOAD__CHANNEL_ID AS "yt_channel_id", m.PAYLOAD__DESCRIPTION  AS "description", 0 AS "yt_category_id", 0 AS "is_zefr_claimed", m.PAYLOAD__DERIVED__AVERAGE_FORECAST AS "forecast", m.PAYLOAD__CHANNEL_TITLE AS "yt_channel_title", s.TAXONOMY_UUID AS "hydra_job_id", s.SCORE as "score" FROM PROD.REPORTING.LATEST_SCORES AS s JOIN PROD.REPORTING.LATEST_MONETIZABLE as m ON s.CONTENT_ID = m.PAYLOAD__CONTENT_ID WHERE TAXONOMY_UUID = \'{}\' AND s.SCORE > {} LIMIT {};'.format(concept_id, min_score, limit)

    print(query)

    QM.make_query(query, "{}.csv".format(slugify(concept_name)))



