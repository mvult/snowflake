import csv
import os

from concepts import slugify

def exclude_country_from_csv(QM, filename, country):

    videos = parse_existing_csv(filename)
    # video_ids = [v['yt_video_id'] for v in videos]

    # subquery = '(SELECT CHANNEL_ID FROM PROD.ANALYST.CHANNEL_STATS WHERE COUNTRY = \'{}\')'.format(country)

    # query = 'SELECT m.PAYLOAD__CONTENT_ID  AS "yt_video_id" FROM PROD.REPORTING.LATEST_MONETIZABLE as m WHERE m.PAYLOAD__CHANNEL_ID IN {} AND m.PAYLOAD__CONTENT_ID IN ({});'.format(subquery, ', '.join(["'{}'".format(c) for c in video_ids]))

    # videos_from_country = QM.make_query_to_list(query) 
    # videos_from_country_dict = {v: True for v in videos_from_country}

    # filtered_videos = [v for v in videos if v['yt_video_id'] not in videos_from_country_dict]
    new_filename = "{} excluded {}.csv".format(slugify(os.path.splitext(filename)[0]), country)
    writer = CSVWriter(new_filename, videos[0].keys())
        # print_to_csv(filtered_videos, new_filename)

    for i in range(0, len(videos), 10000):
        print("Video index currently at: ", i)
        chunk = videos[i:i+10000]
        writer.write(_exclude_country_from_csv(chunk, country, QM))

    writer.close()
    # print(videos_from_india)

def _exclude_country_from_csv(videos, country, QM):
    video_ids = [v['yt_video_id'] for v in videos]

    subquery = '(SELECT CHANNEL_ID FROM PROD.ANALYST.CHANNEL_STATS WHERE COUNTRY = \'{}\')'.format(country)

    query = 'SELECT m.PAYLOAD__CONTENT_ID  AS "yt_video_id" FROM PROD.REPORTING.LATEST_MONETIZABLE as m WHERE m.PAYLOAD__CHANNEL_ID IN {} AND m.PAYLOAD__CONTENT_ID IN ({});'.format(subquery, ', '.join(["'{}'".format(c) for c in video_ids]))

    videos_from_country = QM.make_query_to_list(query) 
    videos_from_country_dict = {v: True for v in videos_from_country}

    return [v for v in videos if v['yt_video_id'] not in videos_from_country_dict]

def parse_existing_csv(filename):
    with open(filename, "r", encoding="utf-8") as f:
        csv_reader = csv.DictReader(f)
        data = [row for row in csv_reader]
    return data


# def print_to_csv(data, filename):
#     with open(filename, 'w+', newline='', encoding="utf-8") as f:
#         writer = csv.DictWriter(f, fieldnames=data[0].keys())
#         writer.writeheader()
#         for row in data:
#             writer.writerow(row)

class CSVWriter:
    def __init__(self, filename, fieldnames):
        self.f = open(filename, 'w+', newline='', encoding="utf-8")
        self.writer = csv.DictWriter(self.f, fieldnames=fieldnames)
        self.writer.writeheader()

    def write(self, data):
        for row in data:
            self.writer.writerow(row)

    def close(self):
        if self.f is not None:
            self.f.close()
        else:
            raise Exception('File is not open. Nothing to close.')