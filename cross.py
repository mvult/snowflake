from dataclasses import dataclass
from package import Package
from concepts import slugify

@dataclass
class CrossRequest:
    isDouble: bool = False
    baseID: str = None
    baseScore: float = None
    base: Package = None
    crossID: str = None
    crossScore: float = None
    cross: Package = None


# E.g., 'cross 234 c332 0.5' or 'cross c12 0.4 c443 0.85'.  The second video group, the 'cross' has to be a concept.  If only for the size issues.
# returns 
def parse_text(text):
    ss = text.split(" ")
    if text.count("C") == 2:
        assert 'C' in ss[1] and 'C' in ss[3]
        return CrossRequest(isDouble=True, baseID=ss[1], baseScore=float(ss[2]), crossID=ss[3], crossScore=float(ss[4]))
    elif text.count('C') == 1:
        return CrossRequest(baseID=ss[1], crossID=ss[2], crossScore=float(ss[3]))
    else:
        raise ValueError("Invalid cross command.  Requires minimum one concept.")


def download_cross_concept(QM, cross_request):
    if cross_request.crossScore < 0.01 or cross_request.crossScore > 1.0:
        raise Exception("Min score for intercept concepts should be at minimum 0.01 or maximum of 100")

    query = 'SELECT m.PAYLOAD__CONTENT_ID  AS "yt_video_id", 0 AS "relevancy_score", m.PAYLOAD__METRICS__VIEWS AS "views", 0 AS "likes", 0 AS "dislikes", 0 AS "comments", m.PAYLOAD__TITLE AS "title", m.PAYLOAD__PUBLISHED_AT AS "publish_ts", m.PAYLOAD__DURATION AS "duration", m.PAYLOAD__CHANNEL_ID AS "yt_channel_id", m.PAYLOAD__DESCRIPTION  AS "description", 0 AS "yt_category_id", 0 AS "is_zefr_claimed", m.PAYLOAD__DERIVED__AVERAGE_FORECAST AS "forecast", 0 AS "yt_channel_title", s.TAXONOMY_UUID AS "hydra_job_id", s.SCORE as "score" FROM PROD.REPORTING.LATEST_SCORES AS s JOIN PROD.REPORTING.LATEST_MONETIZABLE as m ON s.CONTENT_ID = m.PAYLOAD__CONTENT_ID WHERE TAXONOMY_UUID = \'{}\' AND s.SCORE > {} AND ARRAY_CONTAINS({}, m.PAYLOAD__DERIVED__PACKAGE_IDS) LIMIT 2000000;'.format(cross_request.cross.ICV_UUID, cross_request.crossScore, cross_request.base.ID)

    # print(query)

    QM.make_query(query, "{} X {}.csv".format(slugify(cross_request.base.name), slugify(cross_request.cross.icvName)))


# def download_cross_concepts(QM, first_concept_id, first_concept_name, first_concept_score, second_concept_id, second_concept_name, second_concept_score):
def download_cross_concepts(QM, cross_request):

    if cross_request.baseScore < 0.01 or cross_request.baseScore > 1.0:
        raise Exception("Min score should be at minimum 0.01 or maximum of 100")

    if cross_request.crossScore < 0.01 or cross_request.crossScore > 1.0:
        raise Exception("Min score for intercept concepts should be at minimum 0.01 or maximum of 100")

    query = 'SELECT m.PAYLOAD__CONTENT_ID  AS "yt_video_id", 0 AS "relevancy_score", m.PAYLOAD__METRICS__VIEWS AS "views", 0 AS "likes", 0 AS "dislikes", 0 AS "comments", m.PAYLOAD__TITLE AS "title", m.PAYLOAD__PUBLISHED_AT AS "publish_ts", m.PAYLOAD__DURATION AS "duration", m.PAYLOAD__CHANNEL_ID AS "yt_channel_id", m.PAYLOAD__DESCRIPTION  AS "description", 0 AS "yt_category_id", 0 AS "is_zefr_claimed", m.PAYLOAD__DERIVED__AVERAGE_FORECAST AS "forecast", 0 AS "yt_channel_title", s.TAXONOMY_UUID AS "hydra_job_id", s.SCORE as "score" FROM PROD.REPORTING.LATEST_SCORES AS s JOIN PROD.REPORTING.LATEST_MONETIZABLE as m ON s.CONTENT_ID = m.PAYLOAD__CONTENT_ID WHERE TAXONOMY_UUID = \'{}\' AND s.SCORE > {} AND m.PAYLOAD__CONTENT_ID IN (SELECT CONTENT_ID FROM PROD.REPORTING.LATEST_SCORES WHERE TAXONOMY_UUID = \'{}\' AND SCORE > {}) LIMIT 2000000;'.format(cross_request.base.ICV_UUID, cross_request.baseScore, cross_request.cross.ICV_UUID, cross_request.crossScore)

    # print(query)

    QM.make_query(query, "Base {}{} Filtered {}{}.csv".format(slugify(cross_request.base.icvName), int(cross_request.baseScore * 10), slugify(cross_request.cross.icvName), int(cross_request.crossScore * 10)))