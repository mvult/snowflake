import jaydebeapi
import csv

def get_cursor():
	print("Establishing DB connection....")
	conn = jaydebeapi.connect("net.snowflake.client.jdbc.SnowflakeDriver",
	                           "jdbc:snowflake://localhost:10015/?sb=PROD",
	                           {'account':"any", "SSL":"false", "user":"miguel.lavalle", "password":"ms"},
	                           "./snowflake-jdbc-3.12.3.jar")
	cs = conn.cursor()
	cs.execute("USE WAREHOUSE ENGINEER_WH;")
	print("....established!")
	return conn, cs

class QueryManager:
	def __init__(self):
		self.conn, self.cursor = get_cursor()

	def make_query(self, query, file_name=None, fetch_size=10000):
		try:
			self.cursor.execute(query)

			gen = row_generator(self.cursor, fetch_size)
			if file_name is None:
				return gen
			else:
				count = 0
				fp = open(file_name, 'w+', newline='', encoding="utf-8")
				myFile = csv.writer(fp)
				myFile.writerow([i[0] for i in self.cursor.description]) 

				for row in gen:
					myFile.writerow(row)
					count = count + 1
					print(count, end="\r")

				fp.close()
		except Exception as e:
			print(e)
			raise
	
	def make_query_to_list(self, query):
		try:
			self.cursor.execute(query)
			return [row[0] for row in self.cursor.fetchall()]
		except Exception as e:
			print(e)
			raise

	def close(self):
		print("Closing connection")
		self.cursor.close()
		self.conn.close()


def row_generator(cursor, fetch_size):
	tmp = []
	while True:
		if len(tmp) == 0:
			tmp = cursor.fetchmany(fetch_size)
		if len(tmp) == 0:
			return

		yield tmp.pop()






