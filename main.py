import shlex

import click

from cross import download_cross_concept
from java import QueryManager
from package import Package, unique_packages
from concepts import search_concepts, download_concept
from cross import parse_text, download_cross_concept, download_cross_concepts
from channels import get_channel_stats, get_channel_videos
from country import exclude_country_from_csv


def get_package_ids(filename):
	with open(filename.split(" ")[1], "r") as f:
		txt = f.read()

	ids = txt.split(",")
	ids = [int(i.strip('\n')) for i in ids]
	for i in ids:
		assert isinstance(i,int)

	return ids


QM = QueryManager()

class InternalState:
	def __init__(self):
		self.state = "packages"
		self.packages = []

	def setState(self, state):
		if state not in ['initial','packages','confirm']:
			raise "Invalid state"
		self.state = state

	def getPrompt(self):
		if self.state == 'initial':
			return "Package Search"
		elif self.state == 'packages':
			return "count or download or search or list" 
		elif self.state == 'confirm':
			return ""

	def setPackages(self, packages):
		if len(packages) == 0:
			self.packages = packages
		else:
			self.packages.extend(packages)
			self.packages = unique_packages(self.packages)
		
		self.state = 'packages'

	def update_package(self, ID, count):
		self.packages = [Package(p.ID, p.name, count[0]) if ID == p.ID else p for p in self.packages]

	def printPackages(self):
		print("")
		print("")
		print("Concepts")
		print("Concepts    |    Package   |      beta/primary/retired")
		c_id = 1
		for p in self.packages:
			if not p.isConcept: continue
			print("{} {}   || {}    {}/{}/{}".format(p.INTERNAL_CONCEPT_ID, p.icName, p.icvName, p.beta, p.primary, p.retired))

		print("")
		print("")
		print("Keywork Packages")
		for p in self.packages:
			if p.isConcept: continue
			print("{} {} {}".format(p.ID, p.name, p.results) if p.results != 0 else "{} {}".format(p.ID, p.name))

	def get_package(self, ID):
		for p in self.packages:
			try:
				if p.ID == int(ID): return p
			except ValueError:
				pass

			if p.INTERNAL_CONCEPT_ID == ID: return p

		return None


def search_packages(search_term):
	query = "SELECT * FROM PROD.REPORTING.ZEFR_PACKAGES_NAME WHERE LOWER(PACKAGE_NAME) LIKE '%{}%' LIMIT 500;".format(search_term.lower())
	rows = QM.make_query(query)

	ret = []
	for r in rows:
		ret.append(Package(ID= r[0], name=r[1], results= 0))

	if len(ret) > 400:
		print("Warning, over 400 packages.  This is weird.  Did you search right?")

	ret = sorted(ret, key= lambda x: x.name)

	return ret


def get_package_size(package_id):
	query = "SELECT count(*) FROM PROD.REPORTING.LATEST_MONETIZABLE WHERE ARRAY_CONTAINS({}, PROD.REPORTING.LATEST_MONETIZABLE.PAYLOAD__DERIVED__PACKAGE_IDS) LIMIT 1;".format(package_id)

	rows = QM.make_query(query)
	ret = rows.__next__()
	return ret 

def download_file(package_id, name):
	query = 'SELECT PAYLOAD__CONTENT_ID  AS "yt_video_id", 0 AS "relevancy_score", PAYLOAD__METRICS__VIEWS AS "views", 0 AS "likes", 0 AS "dislikes", 0 AS "comments", PAYLOAD__TITLE AS "title", PAYLOAD__PUBLISHED_AT AS "publish_ts", PAYLOAD__DURATION AS "duration", PAYLOAD__CHANNEL_ID AS "yt_channel_id", PAYLOAD__DESCRIPTION  AS "description", 0 AS "yt_category_id", 0 AS "is_zefr_claimed", PAYLOAD__DERIVED__AVERAGE_FORECAST AS "forecast", 0 AS "yt_channel_title", \'CUSTOM_RESEARCH_PULL\' AS "hydra_job_id" FROM PROD.REPORTING.LATEST_MONETIZABLE WHERE ARRAY_CONTAINS({}, PROD.REPORTING.LATEST_MONETIZABLE.PAYLOAD__DERIVED__PACKAGE_IDS) LIMIT 2000000;'.format(package_id)
	QM.make_query(query, "{}.csv".format(name))


def parse_flags(text):
	args = shlex.split(text)
	return {k: True if v.startswith('-') else v for k,v in zip(args, args[1:]+["--"]) if k.startswith('-')}

def main():
	IS = InternalState()
	
	while True:
		try:
			text = click.prompt(IS.getPrompt())
		except click.exceptions.Abort:
			break
		except KeyboardInterrupt:
			break
		except EOFError:
			break

		else:
			if IS.state == "initial":
				packages = search_packages(text)
				concepts = search_concepts(QM, text)
				packages = concepts + packages 
				IS.setPackages(packages)
				IS.printPackages()
			elif IS.state == "packages":

				args = shlex.split(text)
				command = args[0] 
				package_id = args[1] if len(args) > 1 else None
				flags = parse_flags(text) 

				if command == 'search':
					IS.setState('initial')
					continue
				elif command == 'count':
					package_id = int(text.split(" ")[1])
					IS.update_package(package_id, get_package_size(package_id))
					IS.printPackages()
				elif command == "download":

					if 'C' in package_id:
						min_score = float(flags['-rel'])
						p = IS.get_package(package_id)
						print("Trying to download with these parameters.  Entered ID: {}  Concept ID: {}   ICV_UUID: {}    ICV_NAME: {}   MIN_SCORE: {} ".format(package_id, p.INTERNAL_CONCEPT_ID, p.ICV_UUID, p.icvName, min_score))
						print(p)
						download_concept(QM, p.ICV_UUID, p.icvName, min_score, country=flags.get('-country'), limit=flags.get('-limit'))
						return
					else:	
						package_id = int(package_id)
						download_file(package_id, IS.get_package(package_id).name)
				elif command == 'list':
					ids = get_package_ids(text)
					for package_id in ids:
						print(package_id)
						download_file(package_id, package_id)
				elif command == 'details':
					package_id = int(text.split(" ")[1])
					IS.print_package_details(package_id)
				elif command == 'chanStats':
					get_channel_stats(QM, text)
				elif command == 'channels':
					get_channel_videos(QM, text)
				elif command == 'cross':
					cross_request = parse_text(text)
					cross_request.base = IS.get_package(cross_request.baseID)
					cross_request.cross = IS.get_package(cross_request.crossID)

					print(cross_request)

					if cross_request.isDouble:
						download_cross_concepts(QM, cross_request)
					else:
						download_cross_concept(QM, cross_request)
				
				elif command == "exclude":
					exclude_country_from_csv(QM, flags.get('-file'), flags.get('-country'))



	QM.close()
	print('GoodBye!')


if __name__ == '__main__':
	main()