from dataclasses import dataclass

@dataclass
class Package:
    ID: int = 0
    IC_UUID: str = ''
    ICV_UUID: str = ''
    INTERNAL_CONCEPT_ID: str = ''
    name: str = ''
    results: int = 0
    isConcept: bool = False
    icName: str = ''
    icvName: str = ''
    beta: bool = False
    primary: bool = False
    retired: bool = False

def unique_packages(packages):
    ret = []
    concept_count = 1
    conceptMap = set()
    packageMap = set()


    for i, p in enumerate(packages):
        if p.isConcept:
            if p.ICV_UUID in conceptMap:
                continue
            else:
                packages[i].INTERNAL_CONCEPT_ID = 'C'+str(concept_count)
                concept_count = concept_count + 1
                conceptMap.add(p.ICV_UUID)
                ret.append(packages[i])
        else:
            if p.ID in packageMap:
                continue
            else:
                packageMap.add(p.ID)
                ret.append(p)
    return ret

